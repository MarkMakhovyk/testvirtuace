package com.test.virtuace

import com.test.virtuace.domain.ext.parseDate
import com.test.virtuace.domain.ext.toStringISO
import org.junit.Assert
import org.junit.Test
import java.time.LocalDate
import java.time.format.DateTimeParseException

class DateExtTest {

    @Test
    fun stringWithDateIsoParsing() {
        Assert.assertEquals( LocalDate.of(2020, 1, 12), "2020-01-12".parseDate())
    }

    @Test(expected = DateTimeParseException::class)
    fun stringWithWrongDateParsing() {
      "20-01-12".parseDate()
    }

    @Test
    fun dateToStringIsoFormat() {
        Assert.assertEquals( LocalDate.of(2020, 1, 12).toStringISO(), "2020-01-12")
    }
}