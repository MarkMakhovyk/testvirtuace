package com.test.virtuace

import com.test.virtuace.data.model.Couple
import com.test.virtuace.data.CouplesRepository
import com.test.virtuace.domain.GetUpcomingAnniversariesUseCase
import com.test.virtuace.domain.model.AnniversaryType
import kotlinx.coroutines.*
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Test

import org.junit.Before
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.time.LocalDate

@OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
class GetUpcomingAnniversariesUseCaseTest {

    private val repository = mock<CouplesRepository>()

    private lateinit var dispatcher: ExecutorCoroutineDispatcher

    @Before
    fun setUp() {
        dispatcher = newSingleThreadContext("testThread")
    }

    @After
    fun clear() {
       dispatcher.cancel()
    }

    @Test
    fun `check when returned with upcoming list`() = runTest {
        whenever(repository.getCouples()).thenReturn(getCouples())
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val value = useCase(LocalDate.of(2023, 2, 11))
        Assert.assertEquals(0, value.size)
    }

    @Test
    fun `check when returned without upcoming list`() = runTest {
        whenever(repository.getCouples()).thenReturn(getCouples())
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val value = useCase(LocalDate.of(2023, 6, 11))
        Assert.assertEquals(5, value.size)
    }

    @Test
    fun `check when returned empty list`() = runTest {
        whenever(repository.getCouples()).thenReturn(emptyList())
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val value = useCase(LocalDate.of(2023, 2, 11))
        Assert.assertEquals(0, value.size)
    }

    @Test
    fun `check anniv golden type`() {
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val annivType = useCase.getAnniversaryType(
            weddingDate = LocalDate.of(1993, 2, 23),
            anniversaryDate = LocalDate.of(2023, 2, 16)
        )
        Assert.assertEquals(AnniversaryType.Golden, annivType)
    }

    @Test
    fun `check anniv silver type`() {
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val annivType = useCase.getAnniversaryType(
            weddingDate = LocalDate.of(2018, 2, 16),
            anniversaryDate = LocalDate.of(2023, 2, 16)
        )
        Assert.assertEquals(AnniversaryType.Silver, annivType)
    }

    @Test
    fun `check anniv regular type`() {
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val annivType = useCase.getAnniversaryType(
            weddingDate = LocalDate.of(2022, 2, 26),
            anniversaryDate = LocalDate.of(2023, 2, 16)
        )
        Assert.assertEquals(AnniversaryType.Regular, annivType)
    }

    @Test
    fun `check right calculation next anniversary date`() {
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val date = useCase.nextAnniversaryDate(
            currentDate = LocalDate.of(2023, 2, 16),
            weddingDate = LocalDate.of(2022, 2, 26)
        )
        Assert.assertEquals(LocalDate.of(2023, 2, 26), date)
    }

    @Test
    fun `check right calculation next anniversary date 2`() {
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val date = useCase.nextAnniversaryDate(
            currentDate = LocalDate.of(2023, 12, 26),
            weddingDate = LocalDate.of(2020, 12, 29)
        )
        Assert.assertEquals(LocalDate.of(2023, 12, 29), date)
    }

    @Test
    fun `check right calculation next anniversary date 3`() {
        val useCase = GetUpcomingAnniversariesUseCase(repository, dispatcher)
        val date = useCase.nextAnniversaryDate(
            currentDate = LocalDate.of(2023, 12, 26),
            weddingDate = LocalDate.of(2020, 1, 2)
        )
        Assert.assertEquals(LocalDate.of(2024, 1, 2), date)
    }

    private fun getCouples(): List<Couple> = listOf(
        Couple(id = "vlad_viktoria", "2011-12-03"),
        Couple(id = "nic_jessica", "2018-02-23"),
        Couple(id = "rom_julia", "2022-02-26"),
        Couple(id = "sasha_alisa", "1993-02-23"),
        Couple(id = "willy_luba", "1994-02-19"),
        Couple(id = "vova_nata", "2018-02-16"),
        Couple(id = "david_anet", "2017-02-18"),
    )
}