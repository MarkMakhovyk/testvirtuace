package com.test.virtuace.domain.ext

import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun String.parseDate() =
    LocalDate.parse(this, DateTimeFormatter.ISO_LOCAL_DATE)

fun LocalDate.toStringISO() =
    format(DateTimeFormatter.ISO_LOCAL_DATE)