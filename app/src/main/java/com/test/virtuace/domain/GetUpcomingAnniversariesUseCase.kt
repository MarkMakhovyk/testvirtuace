package com.test.virtuace.domain

import com.test.virtuace.data.CouplesRepository
import com.test.virtuace.domain.ext.parseDate
import com.test.virtuace.domain.ext.toStringISO
import com.test.virtuace.domain.model.Anniversaries
import com.test.virtuace.domain.model.AnniversaryType
import kotlinx.coroutines.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class GetUpcomingAnniversariesUseCase(
    private val couplesRepository: CouplesRepository,
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.Default
) {

    suspend operator fun invoke(now:LocalDate = LocalDate.now()): List<Anniversaries> = withContext(defaultDispatcher) {
        val dateInTwoWeek = now.plusWeeks(2)
        couplesRepository.getCouples()
            .map {couple ->
                async { nextAnniversaryDate(now, couple.weddingDate.parseDate()) to couple }
            }
            .awaitAll()
            .filter { (annivDate, _) ->
                annivDate.isBefore(dateInTwoWeek)
            }
            .sortedBy { it.first }
            .map { (annivDate, couple) ->
                async {
                    Anniversaries(
                        couple.id,
                        annivDate.toStringISO(),
                        getAnniversaryType(couple.weddingDate.parseDate(), annivDate)
                    )
                }
            }
            .awaitAll()
    }

    fun nextAnniversaryDate(currentDate: LocalDate, weddingDate: LocalDate): LocalDate {
        val annivYear = if (weddingDate.dayOfYear < currentDate.dayOfYear) {
            currentDate.year + 1
        } else {
            currentDate.year
        }
        return LocalDate.of(annivYear, weddingDate.month, weddingDate.dayOfMonth)
    }

    fun getAnniversaryType(weddingDate: LocalDate, anniversaryDate: LocalDate): AnniversaryType {
        val years = anniversaryDate.year - weddingDate.year
        return when {
            years % 10 == 0 -> AnniversaryType.Golden
            years % 5 == 0 -> AnniversaryType.Silver
            else-> AnniversaryType.Regular
        }
    }
}