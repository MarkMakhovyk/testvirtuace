package com.test.virtuace.domain.model

data class Anniversaries(
    val coupleId: String,
    val anniversariesDate: String,
    val anniversaryType: AnniversaryType
)

enum class AnniversaryType {
    Regular,
    Golden,
    Silver
}