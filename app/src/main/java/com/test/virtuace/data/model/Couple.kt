package com.test.virtuace.data.model

data class Couple(val id: String, val weddingDate: String)