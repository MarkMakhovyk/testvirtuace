package com.test.virtuace.data

import com.test.virtuace.data.model.Couple

interface CouplesRepository {
    suspend fun getCouples(): List<Couple>
}