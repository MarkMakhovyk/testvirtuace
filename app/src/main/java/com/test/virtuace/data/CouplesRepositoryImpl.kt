package com.test.virtuace.data

import com.test.virtuace.data.model.Couple

class CouplesRepositoryImpl: CouplesRepository {

    override suspend fun getCouples(): List<Couple> = listOf(
        Couple(id = "vlad_viktoria", "2011-12-03"),
        Couple(id = "nic_jessica", "2018-02-23"),
        Couple(id = "rom_julia", "2022-02-26"),
        Couple(id = "sasha_alisa", "1993-02-23"),
        Couple(id = "willy_luba", "1994-02-19"),
        Couple(id = "vova_nata", "2018-02-16"),
        Couple(id = "david_anet", "2017-02-18"),
    )
}