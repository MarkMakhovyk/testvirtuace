package com.test.virtuace.ui.theme

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable

private val ColorScheme = lightColorScheme(
    primary = GrayPrimary,
    secondary = GraySecondary,
    background = Background,
    primaryContainer = Green
)

@Composable
fun TestVirtuaceTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colorScheme = ColorScheme,
        typography = Typography,
        content = content
    )
}