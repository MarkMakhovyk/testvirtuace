package com.test.virtuace.ui.theme

import androidx.compose.ui.graphics.Color

val GrayPrimary = Color(0xFF000000)
val GraySecondary = Color(0xFF575757)
val Background = Color(0xFFECECEC)
val Green = Color(0xFF55C595)

val Golden = Color(0xFFFFD700)
val Silver = Color(0xFF808080)
val White = Color(0xFFFFFFFF)
