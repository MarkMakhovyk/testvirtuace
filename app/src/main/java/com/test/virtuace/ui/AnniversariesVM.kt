package com.test.virtuace.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.virtuace.data.CouplesRepositoryImpl
import com.test.virtuace.domain.GetUpcomingAnniversariesUseCase
import com.test.virtuace.domain.model.Anniversaries
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class AnniversariesVM(
    val getUpcomingAnniversaries: GetUpcomingAnniversariesUseCase = GetUpcomingAnniversariesUseCase(
        couplesRepository = CouplesRepositoryImpl()
    )
): ViewModel() {

    private val _anniversaries = MutableStateFlow<List<Anniversaries>?>(null)
    val anniversaries: StateFlow<List<Anniversaries>?> = _anniversaries

    init {
        getAnniversaries()
    }

    private fun getAnniversaries() = viewModelScope.launch {
        _anniversaries.value = getUpcomingAnniversaries()
    }
}