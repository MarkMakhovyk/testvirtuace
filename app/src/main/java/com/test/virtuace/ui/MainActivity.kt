package com.test.virtuace.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.collectAsState
import com.test.virtuace.ui.theme.TestVirtuaceTheme

class MainActivity : ComponentActivity() {
    private val viewModel: AnniversariesVM by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestVirtuaceTheme {
                val state = viewModel.anniversaries.collectAsState()
                AnniversariesScreen(state.value)
            }
        }
    }
}