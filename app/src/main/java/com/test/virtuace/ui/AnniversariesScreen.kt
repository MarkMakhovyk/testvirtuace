package com.test.virtuace.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.test.virtuace.R
import com.test.virtuace.domain.model.Anniversaries
import com.test.virtuace.domain.model.AnniversaryType
import com.test.virtuace.ui.theme.Golden
import com.test.virtuace.ui.theme.Silver
import com.test.virtuace.ui.theme.TestVirtuaceTheme
import com.test.virtuace.ui.theme.White

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AnniversariesScreen(anniversaries: List<Anniversaries>?) {
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        containerColor = MaterialTheme.colorScheme.background,
        topBar = {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colorScheme.secondary)
                    .padding(vertical = 16.dp, horizontal = 16.dp),
                text = stringResource(R.string.upcaming_anniv),
                style = MaterialTheme.typography.titleLarge
                    .copy(
                        textAlign = TextAlign.Center,
                        color = White
                    ),
            )
        }
    ) { paddings ->
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddings)
                .background(color = MaterialTheme.colorScheme.background),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            contentPadding = PaddingValues(
                vertical = 16.dp,
                horizontal = 12.dp
            )
        ) {
            if (anniversaries != null) {
                items(anniversaries) { anniv ->
                    AnniversaryItem(anniv)
                }
            }
        }
    }
}

@Composable
fun AnniversaryItem(anniv: Anniversaries) {
    Column(modifier = Modifier
        .fillMaxWidth()
        .clip(RoundedCornerShape(8.dp))
        .shadow(elevation = 2.dp)
        .background(anniv.anniversaryType.getBackgroundColor())
        .padding(16.dp)
    ) {
        Text(
            text = anniv.coupleId,
            color = MaterialTheme.colorScheme.primary,
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.fillMaxWidth()
        )
        Text(
            text = anniv.anniversariesDate,
            color = MaterialTheme.colorScheme.secondary,
            style = MaterialTheme.typography.bodyMedium,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp)
        )

    }
}

fun AnniversaryType.getBackgroundColor() =
    when (this) {
        AnniversaryType.Golden -> Golden
        AnniversaryType.Silver -> Silver
        AnniversaryType.Regular -> White
    }

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TestVirtuaceTheme {
        AnniversariesScreen(listOf(
            Anniversaries(coupleId = "vlad_viktoria", "2011-12-03", AnniversaryType.Regular),
            Anniversaries(coupleId = "nic_jessica", "2018-02-23", AnniversaryType.Golden),
            Anniversaries(coupleId = "nic_jessica", "2018-02-23", AnniversaryType.Silver),
        ))
    }
}