TASK DESCRIPTION

“You’re welcome to use any programming language (preferably Kotlin) or tools (preferably Android Studio) you wish and you may look up any online resources or references as needed. Espresso tests are welcome but optional.

The details of the test are below.
If anything is unclear, don’t hesitate to contact us and ask questions.

You are given an array of records, each representing a couple. Each record contains the following information:
the couple ID, as a string
the couple’s wedding date, as a string in ISO format (can be null / missing)

Please write a function that, given an array of couple records, creates an array of wedding anniversary reminder records. Each anniversary reminder record should contain the following information:
the couple ID, as a string
the next wedding anniversary date, as a string in ISO format

The function should only create a reminder record for a couple if the next wedding anniversary date is any day within the next 2 weeks.

Please create a sample Android app, which will show anniversary reminders for couples (Input Data can be hardcoded in the source code itself)

Additionally, anniversaries celebrating a multiple of 5 years should be marked with silver color (or similar) and anniversaries celebrating a multiple of 10 years should be marked with golden color (or similar).”

Note: I am more interested on the logic ( So UI has less weightage )